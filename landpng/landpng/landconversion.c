/****************************************************************

landconversion.c

=============================================================

Copyright 1996-2024 Tom Barbalet. All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

This software is a continuing work of Tom Barbalet, begun on
13 June 1996. No apes or cats were harmed in the writing of
this software.

****************************************************************/

#include <stdio.h>

#include "pnglite.h"

extern void land_init( void);
extern void  land_cycle( void );
extern void land_seed_genetics( unsigned short r1, unsigned short  r2 );

extern int write_png_file(char* filename, int width, int height, unsigned char *buffer);

extern const int MAP_DIMENSION;

#define MAP_AREA                      (1<<(2*MAP_BITS))

extern int  topography[2 * 512 * 512];

unsigned char local_topography[512 * 512];

int write_bw_png_file(char* filename, int width, int height, unsigned char *buffer)
{
    png_t png;
    FILE * fp = fopen(filename, "wb");
    if (fp == NULL)
    {
        fprintf(stderr, "Could not open file %s for writing\n", filename);
        return 1;
    }
    fclose(fp);

    png_init(0,0);
    png_open_file_write(&png, filename);
    png_set_data(&png, width, height, 8, PNG_GREYSCALE, buffer);
    png_close_file(&png);

    return 0;
}

int main(int argc, const char * argv[]) {
    int loop = 0;
    land_seed_genetics(16757, 8787);
    land_init();
    while (loop < (512 * 512)){
        local_topography[loop] = topography[loop];
        loop++;
    }    
    write_bw_png_file("mapoutput.png", 512, 512, local_topography);
    return 0;
}
